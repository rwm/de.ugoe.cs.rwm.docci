package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

public class DeployWorkflowTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		CachedResourceSet.getCache().clear();
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(occiPath);
	}

	@After
	public void clearResourceCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void deployWorkflowApplication() throws EolRuntimeException {
		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());
		Logger.getRootLogger().setLevel(Level.FATAL);
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/MLSDiffVM.occic"));

		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));
	}
}
