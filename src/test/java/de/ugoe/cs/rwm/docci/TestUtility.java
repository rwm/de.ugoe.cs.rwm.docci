package de.ugoe.cs.rwm.docci;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.crtp.CrtpPackage;
import org.eclipse.cmf.occi.docker.DockerPackage;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.eclipse.emf.ecore.EObject;
import org.modmacao.ansibleconfiguration.AnsibleconfigurationPackage;
import org.modmacao.occi.platform.PlatformPackage;
import org.modmacao.placement.PlacementPackage;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.docci.appdeployer.MartAppDeployerMaster;
import de.ugoe.cs.rwm.docci.appdeployer.MartAppDeployerSlave;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.extractor.Extractor;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import modmacao.ModmacaoPackage;
import monitoring.MonitoringPackage;
import openstackruntime.OpenstackruntimePackage;
import ossweruntime.OssweruntimePackage;
import workflow.WorkflowPackage;

public class TestUtility {
	public static void extensionRegistrySetup() {
		InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		ModmacaoPackage.eINSTANCE.eClass();
		OpenstackruntimePackage.eINSTANCE.eClass();
		PlacementPackage.eINSTANCE.eClass();
		WorkflowPackage.eINSTANCE.eClass();
		OssweruntimePackage.eINSTANCE.eClass();
		AnsibleconfigurationPackage.eINSTANCE.eClass();
		PlatformPackage.eINSTANCE.eClass();
		MonitoringPackage.eINSTANCE.eClass();
		CrtpPackage.eINSTANCE.eClass();
		DockerPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://occiware.org/occi/docker#",
				OCCIPackage.class.getClassLoader().getResource("model/docker.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/platform#",
				PlatformPackage.class.getClassLoader().getResource("model/platform.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/modmacao#",
				ModmacaoPackage.class.getClassLoader().getResource("model/modmacao.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/runtime#",
				OCCIPackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OCCIPackage.class.getClassLoader().getResource("model/ossweruntime.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/placement#",
				PlacementPackage.class.getClassLoader().getResource("model/placement.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure#",
				InfrastructurePackage.class.getClassLoader().getResource("model/Infrastructure.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://occiware.org/occi/docker#",
				InfrastructurePackage.class.getClassLoader().getResource("model/docker.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/core#",
				OCCIPackage.class.getClassLoader().getResource("model/Core.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure/compute/template/1.1#",
				OCCIPackage.class.getClassLoader().getResource("model/crtp.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/workflow#",
				WorkflowPackage.class.getClassLoader().getResource("model/workflow.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/monitoring#",
				OCCIPackage.class.getClassLoader().getResource("model/monitoring.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OCCIPackage.class.getClassLoader().getResource("model/ossweruntime.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/ansible#",
				AnsibleconfigurationPackage.class.getClassLoader().getResource("model/ansibleconfiguration.occie")
						.toString());
	}

	public static void loggerSetup() {
		File log4jfile = new File(ModelUtility.getPathToResource("log4j.properties"));
		PropertyConfigurator.configure(log4jfile.getAbsolutePath());
		Logger.getLogger(Executor.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(Extractor.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(Provisioner.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(Deprovisioner.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Connector.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(Deployer.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(MartAppDeployerMaster.class.getName()).setLevel(Level.DEBUG);
		Logger.getLogger(MartAppDeployerSlave.class.getName()).setLevel(Level.DEBUG);
	}

	public static boolean equalsRuntime(Path desiredModelPath, Connector conn) {
		Path deployedOCCI = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
		conn.loadRuntimeModel(deployedOCCI);
		org.eclipse.emf.ecore.resource.Resource desiredModel = ModelUtility.loadOCCIintoEMFResource(desiredModelPath);
		org.eclipse.emf.ecore.resource.Resource runtimeModel = ModelUtility.loadOCCIintoEMFResource(deployedOCCI);

		Comparator comp = ComparatorFactory.getComparator("Simple", desiredModel, runtimeModel);

		boolean assertion = true;
		System.out.println("MISSING ELEMENTS:");
		for (EObject obj : comp.getMissingElements()) {
			// Network check due to provider network
			if (obj.eClass().getName().equals("Network") == false) {

				System.out.println(obj);
				assertion = false;
			}

		}

		System.out.println("NEW ELEMENTS:");
		for (EObject obj : comp.getNewElements()) {
			// Network check due to provider network
			if (obj.eClass().getName().equals("Network") == false) {
				System.out.println(obj);
				assertion = false;
			}
		}
		return assertion;
	}

}
