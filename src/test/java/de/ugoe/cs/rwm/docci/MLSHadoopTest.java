package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

public class MLSHadoopTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		CachedResourceSet.getCache().clear();
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(occiPath);
	}

	@Test
	public void mls_hadoop() throws EolRuntimeException {
		Logger.getRootLogger().setLevel(Level.FATAL);
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/MLS_Hadoop.occic"));
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		Resource workflowModel = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(workflowModel);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));

	}
}
