package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.history.DocciHistory;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

public class JobHistoryTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		CachedResourceSet.getCache().clear();
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(occiPath);

	}

	@Test
	public void jobHistoryTest() throws EolRuntimeException, IOException {
		Logger.getRootLogger().setLevel(Level.FATAL);
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/MLS_Hadoop.occic"));
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		Resource workflowModel = ModelUtility.loadOCCIintoEMFResource(occiPath);

		File f = new File("job_history");
		deleteDirectory(f);
		assertTrue(f.exists() == false);

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.setJobHistoryPath("job_history");
		deployer.deploy(workflowModel);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));

		assertTrue(f.exists() == true);
		assertTrue(f.isDirectory());

		for (File entry : f.listFiles()) {
			assertEquals(5, entry.listFiles().length);
			for (File e : entry.listFiles()) {
				System.out.println(e);
				if (e.getName().equals("job.json")) {
					DocciHistory dhist = (DocciHistory) extractHistory(e, DocciHistory.class);
					System.out.println(dhist.getDuration());
					// TODO: extend test case to check correct calculation of durations
				}
			}
		}

		deleteDirectory(f);
		assertTrue(f.exists() == false);

	}

	boolean deleteDirectory(File directoryToBeDeleted) {
		File[] allContents = directoryToBeDeleted.listFiles();
		if (allContents != null) {
			for (File file : allContents) {
				deleteDirectory(file);
			}
		}
		return directoryToBeDeleted.delete();
	}

	private Object extractHistory(File jobJson, Class clazz) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Object hist = objectMapper.readValue(jobJson, clazz);
		return hist;
	}
}
