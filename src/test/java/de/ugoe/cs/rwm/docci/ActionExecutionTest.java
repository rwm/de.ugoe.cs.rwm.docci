package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Action;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.executor.ExecutorFactory;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

public class ActionExecutionTest {

	String manNWid = "urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590";
	String manNWRuntimeId = "75a4639e-9ce7-4058-b859-8a711b0e2e7b";
	String sshKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova";
	String userData = "I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj";

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);

		deployer.deploy(occiPath);
	}

	@After
	public void clearResourceCache() {
		CachedResourceSet.getCache().clear();
	}

	// @Test
	public void deployWaaSArchitecture() throws EolRuntimeException {

		Logger.getRootLogger().setLevel(Level.FATAL);

		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/WaaSAction.occic"));
		org.eclipse.emf.ecore.resource.Resource model = ModelUtility.loadOCCIintoEMFResource(occiPath);
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		trans.transform(occiPath, occiPath);
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(model);

		Resource task = getFirstTask(occiPath);

		Action start = getStartAction(task);
		Executor exec = ExecutorFactory.getExecutor("Mart", conn);
		exec.executeOperation("POST", task, start);

		assertTrue(true);
	}

	private Resource getFirstTask(Path occiPath) {
		EList<EObject> model = ModelUtility.loadOCCI(occiPath);
		for (EObject obj : model) {
			if (obj instanceof Resource) {
				Resource res = (Resource) obj;
				if (res.getKind().getTerm().equals("task")) {
					return res;
				}
			}
		}
		return null;
	}

	private static Action getStartAction(Resource task) {
		for (Action act : task.getKind().getActions()) {
			if (act.getTerm().equals("start")) {
				return act;
			}
		}
		throw new NoSuchElementException("Start action not found");
	}
}
