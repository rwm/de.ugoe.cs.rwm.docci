package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.MartConnector;

public class ConnectorTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void MartConnectorGet() {
		Connector conn = new MartConnector("localhost", 8080, "erbel", "keyPath");
		assertEquals("localhost", conn.getAddress());
		assertEquals(8080, conn.getPort());
		assertEquals("erbel", conn.getUser());
	}

	@Test
	public void MartConnectorSet() {
		Connector conn = new MartConnector("localhost", 8080, "erbel", "keyPath");

		conn.setPort(12);
		conn.setAddress("192.168.145.2");
		conn.setUser("john");

		assertEquals("192.168.145.2", conn.getAddress());
		assertEquals(12, conn.getPort());
		assertEquals("john", conn.getUser());
	}
}
