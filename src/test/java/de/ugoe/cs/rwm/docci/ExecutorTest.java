package de.ugoe.cs.rwm.docci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.executor.MartExecutor;

public class ExecutorTest {
	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void deprovisionEverything() {
		CachedResourceSet.getCache().clear();
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(occiPath);
	}

	@Ignore
	public void provisionAttributeWithComma() {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/comma.occic"));
		Resource res = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Configuration config = ModelUtility.getOCCIConfiguration(res);

		org.eclipse.cmf.occi.core.Resource test = config.getResources().get(0);

		Logger.getRootLogger().setLevel(Level.FATAL);

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartExecutor exec = new MartExecutor(conn);

		System.out.println(test.getAttributes());
		exec.executePutOperation(test);

		assertTrue(true);
	}

	@Ignore
	public void jsonTestMixin() {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/comma.occic"));
		Resource res = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Configuration config = ModelUtility.getOCCIConfiguration(res);

		org.eclipse.cmf.occi.core.Resource test = config.getResources().get(1);

		Logger.getRootLogger().setLevel(Level.FATAL);

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartExecutor exec = new MartExecutor(conn);

		System.out.println(test.getAttributes());
		exec.executePutOperation(test);

		assertTrue(true);
	}

	@Test
	public void jsonTestDeployment() {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/comma.occic"));
		Resource res = ModelUtility.loadOCCIintoEMFResource(occiPath);

		Configuration config = ModelUtility.getOCCIConfiguration(res);

		Link link = null;
		org.eclipse.cmf.occi.core.Resource comp = config.getResources().get(0);
		if (comp.getLinks().isEmpty() == false) {
			link = comp.getLinks().get(0);
		}
		org.eclipse.cmf.occi.core.Resource net = config.getResources().get(1);
		if (net.getLinks().isEmpty() == false) {
			link = comp.getLinks().get(0);
		}
		Logger.getRootLogger().setLevel(Level.FATAL);

		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartExecutor exec = new MartExecutor(conn);

		exec.executePutOperation(comp);
		exec.executePutOperation(net);
		exec.executePutOperation(link);

		assertTrue(TestUtility.equalsRuntime(occiPath, conn));
	}
}
