package de.ugoe.cs.rwm.docci;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class XMLCharacterTest {
	private static final Path RUNTIMEPATH = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	private final Connector CONN = new LocalhostConnector("localhost", 8080, "ubuntu");

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
		Logger.getRootLogger().setLevel(Level.FATAL);
	}

	@Before
	public void deprovisionEverything() {
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		MartDeployer deployer = new MartDeployer(CONN);
		deployer.deploy(occiPath);
	}

	@After
	public void clearResourceCache() {
		CachedResourceSet.getCache().clear();
	}

	@Test
	public void checkEquals() {
		testSpecialCharInModel("occi/specialcharacters/equal.occic");
	}

	@Test
	public void checkAnd() {
		testSpecialCharInModel("occi/specialcharacters/and.occic");
	}

	@Test
	public void checkLT() {
		testSpecialCharInModel("occi/specialcharacters/lessthan.occic");
	}

	@Test
	public void checkGT() {
		testSpecialCharInModel("occi/specialcharacters/largerthan.occic");
	}

	@Test
	public void checkQuote() {
		testSpecialCharInModel("occi/specialcharacters/quote.occic");
	}

	private void testSpecialCharInModel(String occiModel){
		Path occiPath = Paths.get(ModelUtility.getPathToResource(occiModel));
		MartDeployer deployer = new MartDeployer(CONN);
		deployer.deploy(occiPath);

		System.out.println("Design: " + getMessageFromCompute(occiPath));
		System.out.println("Run: " + getMessageFromCompute(RUNTIMEPATH));
		assertEquals(getMessageFromCompute(occiPath), getMessageFromCompute(RUNTIMEPATH));
	}


	private String getMessageFromCompute(Path path){
		CONN.loadRuntimeModel(RUNTIMEPATH);
		Configuration runtimeModel = ModelUtility.loadOCCIConfiguration(path);
		for(Resource res: runtimeModel.getResources()){
			if(res instanceof Compute){
				if(res.getSummary() != null){
					return res.getSummary();
				} else{
					for(AttributeState as: res.getAttributes()){
						if(as.getName().contains("summary")){
							return as.getValue();
						}
					}
				}

			}
		}
		return "";
	}
}
