/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.extractor;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.ActivityNode;

/**
 * Interface for the extraction of elements to be provisioned out of a topology
 * Model.
 *
 * @author erbel
 *
 */
public interface Extractor {
	static Logger LOG = Logger.getLogger(Extractor.class.getName());

	/**
	 * Extracts corresponding element in the topology model of the passed activity.
	 * Therefore it compares the ID in the value of the ValuePin of the activity
	 * diagram with the IDs of the elements in the topology model.
	 *
	 * @param activity
	 * @return Corresponding element in the topology model of the passed activity.
	 */
	public EObject extractElement(ActivityNode activity, EList<EObject> model);
}
