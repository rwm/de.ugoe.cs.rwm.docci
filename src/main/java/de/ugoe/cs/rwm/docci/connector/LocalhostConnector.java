/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.connector;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.retriever.ModelRetriever;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

/**
 * Connector user for a Server running on the same Host.
 *
 * @author erbel
 *
 */
public class LocalhostConnector extends AbsConnector {
	private static final Path LOCAL_RUNTIMEMODEL = Paths
			.get(System.getProperty("user.home") + "/models/model-anonymous.occic");

	/**
	 * Constructor creating a LocalhostConnector.
	 *
	 * @param address
	 *            Address of the LocalhostConnector.
	 * @param port
	 *            Port used by the OCCI API.
	 * @param user
	 *            Name of the user to be used.
	 */
	public LocalhostConnector(String address, int port, String user) {
		this.address = address;
		this.port = port;
		this.user = user;
	}

	@Override
	public Path loadRuntimeModel(Path lfile) {
		CachedResourceSet.getCache().clear();
		Resource res = createLocalCopy(lfile);
		performTransformation(res, lfile);
		LOG.info("Runtime Model stored at: " + lfile.toAbsolutePath());
		return lfile;
	}

	private void performTransformation(Resource res, Path lfile) {
		LOG.info("Transforming Runtime Model");
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		try {
			trans.transform(res, lfile);
		} catch (EolRuntimeException e) {
			LOG.fatal("An error occured when trying to transform the runtime model with the OCCI2OCCI Transformation."
					+ "Returning the original runtime model instead");
		}
	}

	private Resource createLocalCopy(Path lfile) {
		ModelRetriever.refreshMartRuntimeModel(this.address, this.port);
		retrieveRuntimeModel(lfile);
		Resource res = null;
		try {
			res = ModelUtility.loadOCCIintoEMFResource(lfile);
		} catch (Exception e){
			LOG.fatal("Runtime model could not be correctly loaded. Retrying!");
			ModelRetriever.refreshMartRuntimeModel(this.address, this.port);
			retrieveRuntimeModel(lfile);
			res = ModelUtility.loadOCCIintoEMFResource(lfile);
		}
		return res;
	}

	private void retrieveRuntimeModel(Path lfile) {
		LOG.debug("Loading runtime model into: " + lfile.toAbsolutePath());
		try {
			LOG.debug("File: " + lfile);
			Files.copy(LOCAL_RUNTIMEMODEL, lfile, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			LOG.error("An error occured copying the runtime model to " + lfile);
			e.printStackTrace();
		}

	}
}
