/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.provisioner;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.ActivityNode;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.executor.Executor;

/**
 * Factory returning the desired Provisioner.
 *
 * @author erbel
 *
 */
public class ProvisionerFactory {

	/**
	 * Returns provisioner Object of the specified criteria, which starts at the
	 * specified startNode, uses the specified executor and occiModel.
	 *
	 * @param criteria
	 *            Provisioner object to be used.
	 * @param startNode
	 *            Start point in the activity diagram.
	 * @param occiModel
	 *            OCCI Model to be provisioned.
	 * @param exec
	 *            Executor to be used.
	 * @param conn
	 *            Connector to be used.
	 * @return Returns provisioner with the specified parameters.
	 */
	public static Provisioner getProvisioner(String criteria, ActivityNode startNode, EList<EObject> occiModel,
			Executor exec, Connector conn) {
		if (criteria.equals("Mart")) {
			return new MartProvisioner(startNode, exec, occiModel);
		}
		return null;
	}
}
