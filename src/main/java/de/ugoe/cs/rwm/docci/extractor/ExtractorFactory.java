/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.extractor;

/**
 * Factory class for the creation of Extractor objects. These extractor handle
 * the extraction of information from specific models and their elements.
 *
 * @author erbel
 *
 */
public class ExtractorFactory {
	/**
	 * Returns Extractor instance.
	 *
	 * @param criteria
	 *            Accepts: "OCCI"
	 * @return Model Extractor.
	 */
	public static Extractor getExtractor(String criteria) {
		if (criteria.equals("OCCI")) {
			return new OCCIExtractor();
		}
		return null;
	}
}
