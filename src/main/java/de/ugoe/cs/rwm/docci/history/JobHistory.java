package de.ugoe.cs.rwm.docci.history;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JobHistory extends AbsHistory {
	protected String jobName;
	protected String book;
	protected String mode;
	protected String jobId;
	protected String status;

	public JobHistory() {
	}

	public void storeHistory(String jobFolder) {
		try {
			convertToJson(jobFolder);
		} catch (IOException e) {
			System.out.println("Could not store History.");
			e.printStackTrace();
		}
	}

	protected void convertToJson(String jobFolder) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.writeValue(new File(jobFolder + "/job.json"), this);
	}

	protected long calculateDuration(Date endDate, Date startDate, TimeUnit s) {
		if (endDate != null && startDate != null) {
			long durationInMilliSeconds = Math.abs(endDate.getTime() - startDate.getTime());
			return s.convert(durationInMilliSeconds, TimeUnit.MILLISECONDS);
		}
		return 0;
	}

	public String getJobName() {
		return jobName;
	}

	public String getJobId() {
		return jobId;
	}

	public String getStatus() {
		return status;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
}
