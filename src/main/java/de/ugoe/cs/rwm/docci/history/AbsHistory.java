package de.ugoe.cs.rwm.docci.history;

import java.util.Date;
import java.util.concurrent.TimeUnit;

;

public class AbsHistory {
	protected Date readableStartDate;
	protected Date readableEndDate;
	protected long startDate;
	protected long endDate;
	protected long duration;
	protected String readableDuration;

	protected long calcDurationInS(long end, long start) {
		return TimeUnit.SECONDS.convert(Math.abs(end - start), TimeUnit.MILLISECONDS);
	}

	protected long calcDurationInMS(long end, long start) {
		return TimeUnit.MILLISECONDS.convert(Math.abs(end - start), TimeUnit.MILLISECONDS);
	}

	protected String formatToReadableDuration(long duration) {
		return String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(duration),
				TimeUnit.MILLISECONDS.toSeconds(duration)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public Date getReadableStartDate() {
		Date d = readableStartDate;
		return d;
	}

	public void setReadableStartDate(Date readableStartDate) {
		this.readableStartDate = (Date) readableStartDate.clone();
	}

	public Date getReadableEndDate() {
		Date d = readableEndDate;
		return d;
	}

	public void setReadableEndDate(Date readableEndDate) {
		this.readableEndDate = (Date) readableEndDate.clone();
	}

	public String getReadableDuration() {
		return readableDuration;
	}

	public void setReadableDuration(String readableDuration) {
		this.readableDuration = readableDuration;
	}
}
