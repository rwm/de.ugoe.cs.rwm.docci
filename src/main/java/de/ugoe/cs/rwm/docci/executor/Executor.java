/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.executor;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;

/**
 * Interface for the communication to the cloud provider.
 *
 * @author erbel
 *
 */
public interface Executor {
	static Logger LOG = Logger.getLogger(Executor.class.getName());

	/**
	 * Handles the concrete execution to provision a cloud resource as specified in
	 * an EMF Object.
	 *
	 * @param element
	 *            containing the element to be provisioned.
	 */
	public String executeOperation(String operation, EObject element, EObject action);

	/**
	 * Returns session token as string for the given parameters.
	 *
	 * @param user
	 *            User of the session.
	 * @param password
	 *            Password of the user.
	 * @param project
	 *            Project of the user.
	 * @param authenticationAdress
	 *            Address of the Authentication server.
	 * @return String containing the Token.
	 */
	@Deprecated
	public String createToken(String user, String password, String project, String authenticationAdress);

}
