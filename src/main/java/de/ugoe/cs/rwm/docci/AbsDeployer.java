/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.uml2.uml.Model;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import pog.Edge;
import pog.Graph;
import pog.Vertex;
import pog.util.Utility;

/**
 * Abstract Deployer implementing the Deployer Interface providing general
 * methods required by each deployer.
 *
 * @author erbel
 *
 */
public abstract class AbsDeployer implements Deployer {
	static Logger log = Logger.getLogger(Deployer.class.getName());

	protected Connector conn;
	protected static final Path RUNTIMEPATH = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	protected static final Path POGPATH = Paths.get(System.getProperty("user.home") + "/.rwm/POG.pog");
	protected static final Path PROVPLANPATH = Paths
			.get(System.getProperty("user.home") + "/.rwm/ProvisioningPlan.uml");
	protected String jobhistorypath;

	public AbsDeployer() {
		boolean success = new File(System.getProperty("user.home") + "/.rwm/").mkdirs();
		if (success) {
			log.info(".rwm folder created under user.home");
		}
	}

	/**
	 * Method used to generate a provisioning plan for the Model stored in
	 * newModelPath. Hereby a POG is created from which the oldElements are
	 * subtracted, followed by a transformation into the provisioning plan.
	 * (OldElements already exist on the Cloud and therefore must not be created
	 * anymore, it follows that they are not allowed in the provisioning plan to be
	 * created and therefore must be subtracted. Furthermore, due to the fact that
	 * they already exist, every dependency typically depicted in the POG is already
	 * resolved.)
	 *
	 * @param targetModelResource Model from which the provisioning plan is
	 *                            generated
	 * @param oldElements         Elements to be deleted from the POG.
	 * @return provisioningPlan to be executed by the provisioner.
	 */
	protected Model generateProvisioningPlan(org.eclipse.emf.ecore.resource.Resource targetModelResource,
			List<EObject> oldElements) {
		// Generate POGs
		Transformator occiToPog = TransformatorFactory.getTransformator("OCCI2POG");
		try {
			occiToPog.transform(targetModelResource, POGPATH);
		} catch (EolRuntimeException e) {
			log.error("OCCI model could not be transformed into a POG " + e);
		}
		EList<EObject> newPOG = Utility.loadPOG(POGPATH);
		Graph newPOGGraph = (Graph) newPOG.get(0);

		List<Vertex> toRemove = new BasicEList<Vertex>();
		for (Vertex vertex : newPOGGraph.getVertices()) {
			for (EObject old : oldElements) {
				if (((Entity) old).getId().equals(vertex.getId())) {
					toRemove.add(vertex);
				}
			}
		}
		for (Vertex vertex : toRemove) {
			EcoreUtil.delete(vertex);
		}

		List<Edge> toRemoveE = new BasicEList<Edge>();
		for (Edge edge : newPOGGraph.getEdges()) {
			if (edge.getTarget() == null || edge.getSource() == null) {
				toRemoveE.add(edge);
			}
		}
		for (Edge edge : toRemoveE) {
			EcoreUtil.delete(edge);
		}

		Utility.storePOG(POGPATH, newPOGGraph);

		// newPOG = Utility.loadPOG(POGPATH);
		// newPOGGraph = (Graph) newPOG.get(0);

		// Clear Resource Cache
		CachedResourceSet.getCache().clear();

		// Generate provisioning plan
		Transformator pogToProvPlan = TransformatorFactory.getTransformator("POG2ProvPlan");
		try {
			pogToProvPlan.transform(POGPATH, PROVPLANPATH);
		} catch (EolRuntimeException e) {
			log.error("POG model could not be transformed into a Provisioning plan " + e);
		}

		Model provisioningPlan = ModelUtility.loadUML(PROVPLANPATH);
		return provisioningPlan;
	}

	protected void storeModelsForHistory(String dir, Resource targetModel, Resource runtimeModel) throws IOException {
		targetModel.setURI(URI.createFileURI(dir.toString() + "/Target.occic"));
		targetModel.save(Collections.EMPTY_MAP);
		runtimeModel.setURI(URI.createFileURI(dir.toString() + "/Run.occic"));
		runtimeModel.save(Collections.EMPTY_MAP);
	}

	public void setJobHistoryPath(String jhs) {
		this.jobhistorypath = jhs;
	}
}
