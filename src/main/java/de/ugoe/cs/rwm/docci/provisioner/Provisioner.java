/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.provisioner;

public interface Provisioner {
	/**
	 * Starts the provisioning process.
	 */
	void provisionElements();

	/**
	 * Sets the path to store job specfic metrics.
	 */
	void setJobHistoryPath(String jhs);

}
