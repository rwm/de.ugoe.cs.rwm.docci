/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.extractor;

import java.util.NoSuchElementException;

import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ValuePin;

import de.ugoe.cs.rwm.docci.ModelUtility;

/**
 * Extractor used to extract information from a OCCI Topology.
 *
 * @author erbel
 *
 */
public class OCCIExtractor implements Extractor {
	@Override
	public Entity extractElement(ActivityNode activity, EList<EObject> model) {
		String id = getInputIDOfElement(activity);
		Entity entity = findCorrespondingElement(id, model);
		return entity;
	}

	/**
	 * Compares every Element in the OCCI Model to the passed id.
	 *
	 * @param id
	 * @param occi
	 * @return Element possessing the passed id.
	 */
	private Entity findCorrespondingElement(String id, EList<EObject> occi) {
		for (EObject element : ModelUtility.getEntities(occi)) {
			Entity entity = (Entity) element;
			if (id.equals(entity.getId())) {
				return entity;
			}
		}
		LOG.error("Element could not be found in topology model: " + id);
		return null;
	}

	/**
	 * Returns ID contained in the ValuePin of the activity
	 *
	 * @param activity
	 * @return ID of the element searched for.
	 */
	private String getInputIDOfElement(ActivityNode activity) {
		for (EObject obj : activity.eContents()) {
			if (obj instanceof ValuePin) {
				return ((ValuePin) obj).getValue().stringValue();
			}
		}
		throw new NoSuchElementException(
				"ActivityNode is not an Action or does not store an id: " + activity.getName());
	}
}
