/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.connector;

import java.nio.file.Path;

/**
 * Interface responsible for the connection to an OCCI API.
 *
 * @author erbel
 *
 */
public interface Connector {

	/**
	 * Returns the port of the connection.
	 *
	 * @return port Current Port of the Connector.
	 */
	public int getPort();

	/**
	 * Sets the port of the connection.
	 *
	 * @param port
	 *            Desired Port of the Connector.
	 */
	public void setPort(int port);

	/**
	 * Returns the address of the Connector.
	 *
	 * @return address Current address of the Connector.
	 */
	public String getAddress();

	/**
	 * Sets the address of the Connector.
	 *
	 * @param address
	 *            Desired address of the Connector.
	 */
	public void setAddress(String address);

	/**
	 * Returns user of the Connector.
	 *
	 * @return user Current username of the Connector.
	 */
	public String getUser();

	/**
	 * Sets the cloud user of the connection;
	 *
	 * @param user
	 *            Desired username of the Connector.
	 */
	public void setUser(String user);

	/**
	 * Loads the runtime model stored for the specified cloud and user and stores it
	 * into the location specified by lfile.
	 *
	 * @param lfile
	 *            Desired path in which the runtime model gets stored.
	 * @return path Path to the stored runtime model.
	 */
	public Path loadRuntimeModel(Path lfile);

}
