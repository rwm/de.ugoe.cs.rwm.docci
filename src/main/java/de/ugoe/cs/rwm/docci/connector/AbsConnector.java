/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.connector;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

/**
 * Abstract Class implementing the Connector Interface, providing general
 * methods required by subclasses.
 *
 * @author erbel
 *
 */
public abstract class AbsConnector implements Connector {
	static Logger LOG = Logger.getLogger(Connector.class.getName());
	protected String project;
	protected String publicNetworkId;
	protected String address;
	protected String user;
	protected int port;

	/**
	 * Checks whether a connection is reachable
	 *
	 * @return boolean Showing whether the API is reachable or not.
	 */
	public boolean isReachable() {
		HttpURLConnection.setFollowRedirects(false);
		HttpURLConnection conn = null;
		try {
			URL url = new URL("http://" + this.address + ":" + this.port);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("HEAD");
			int responseCode = conn.getResponseCode();
			if (responseCode > 100) {
				return true;
			}
		} catch (MalformedURLException e) {
			LOG.info("Connection can not be established: " + "http://" + this.address + ":" + this.port);
			return false;
		} catch (IOException e) {
			LOG.info("Connection can not be established: " + "http://" + this.address + ":" + this.port);
			return false;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return false;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String adress) {
		this.address = adress;
	}

	public String getPublicNetworkId() {
		return publicNetworkId;
	}

	public void setPublicNetworkId(String publicNetworkId) {
		this.publicNetworkId = publicNetworkId;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
