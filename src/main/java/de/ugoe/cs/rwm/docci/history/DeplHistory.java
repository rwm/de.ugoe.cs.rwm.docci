package de.ugoe.cs.rwm.docci.history;

import java.util.Date;

import org.eclipse.cmf.occi.core.Entity;

public class DeplHistory extends EntHistory {
	private long deplStart;
	private long deplEnd;
	private long deplDuration;
	private long confStart;
	private long confEnd;
	private long confDuration;
	private long startStart;
	private long startEnd;
	private long startDuration;

	public DeplHistory(Entity ent, long deplStart, long deplEnd, long confStart, long confEnd, long startStart,
			long startEnd) {

		this.startDate = deplStart;
		this.endDate = startEnd;
		this.duration = calcDurationInMS(startEnd, deplStart);

		this.readableStartDate = new Date(deplStart);
		this.readableEndDate = new Date(startEnd);

		this.entId = ent.getId();
		this.entName = ent.getTitle();

		this.deplStart = deplStart;
		this.deplEnd = deplEnd;
		this.deplDuration = calcDurationInMS(deplEnd, deplStart);

		this.confStart = confStart;
		this.confEnd = confEnd;
		this.confDuration = calcDurationInMS(confEnd, confStart);

		this.startStart = startStart;
		this.startEnd = startEnd;
		this.startDuration = calcDurationInMS(startEnd, startStart);
	}

	public DeplHistory() {

	}

	public long getDeplStart() {
		return deplStart;
	}

	public void setDeplStart(long deplStart) {
		this.deplStart = deplStart;
	}

	public long getDeplEnd() {
		return deplEnd;
	}

	public void setDeplEnd(long deplEnd) {
		this.deplEnd = deplEnd;
	}

	public long getDeplDuration() {
		return deplDuration;
	}

	public void setDeplDuration(long deplDuration) {
		this.deplDuration = deplDuration;
	}

	public long getConfStart() {
		return confStart;
	}

	public void setConfStart(long confStart) {
		this.confStart = confStart;
	}

	public long getConfEnd() {
		return confEnd;
	}

	public void setConfEnd(long confEnd) {
		this.confEnd = confEnd;
	}

	public long getConfDuration() {
		return confDuration;
	}

	public void setConfDuration(long confDuration) {
		this.confDuration = confDuration;
	}

	public long getStartStart() {
		return startStart;
	}

	public void setStartStart(long startStart) {
		this.startStart = startStart;
	}

	public long getStartEnd() {
		return startEnd;
	}

	public void setStartEnd(long startEnd) {
		this.startEnd = startEnd;
	}

	public long getStartDuration() {
		return startDuration;
	}

	public void setStartDuration(long startDuration) {
		this.startDuration = startDuration;
	}
}
