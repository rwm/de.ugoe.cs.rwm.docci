/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.ComputeStatus;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.uml2.uml.Model;
import org.json.JSONArray;
import org.json.JSONObject;
import org.modmacao.occi.platform.Application;

import com.google.common.io.Files;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.docci.appdeployer.MartAppDeployerMaster;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.containerrunner.MartContainerRunnerMaster;
import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.executor.ExecutorFactory;
import de.ugoe.cs.rwm.docci.executor.MartExecutor;
import de.ugoe.cs.rwm.docci.history.DocciHistory;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import de.ugoe.cs.rwm.docci.provisioner.ProvisionerFactory;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;

/**
 * Deployer specifically designed for the MART server.
 *
 * @author erbel
 *
 */
public class MartDeployer extends AbsDeployer {
	private final static String COMP_TYPE = "Simple";
	private final static String EXEC_TYPE = "Mart";
	private final static String PROV_TYPE = "Mart";
	private int configTime = 0;
	final static private String MANAGEMENTNETWORK = "urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590";

	/**
	 * Constructor for the MartDeployer.
	 *
	 * @param conn Connector to be used.
	 */
	public MartDeployer(Connector conn) {
		super();
		this.conn = conn;
	}

	public MartDeployer(Connector conn, int configTime) {
		super();
		this.conn = conn;
		this.configTime = configTime;
	}

	/**
	 * Deployment method taking an EMFResource of the OCCI model as input.
	 *
	 * @param targetModel EMFResource of the OCCI model to be deployed.
	 */
	public void deploy(org.eclipse.emf.ecore.resource.Resource targetModel) {
		Date startProvDate = new Date();
		org.eclipse.emf.ecore.resource.Resource runtimeModel = downloadRuntimeModel();
		registerUserMixins(targetModel);

		if (jobhistorypath != null) {
			this.setJobHistoryPath(jobhistorypath + "/job_" + startProvDate.getTime());
		}

		if (ModelUtility.getOCCIConfigurationContents(runtimeModel).size() == 0) {
			log.info("Chosen: Initial Deployment, Amount of Resources in Runtimemodel: "
					+ ModelUtility.getOCCIConfigurationContents(runtimeModel).size());
			this.initialDeploy(targetModel);
		} else {
			log.info("Chosen: Adaptation Process");
			this.adapt(runtimeModel, targetModel);
		}
		Date endProvDate = new Date();

		if (jobhistorypath != null) {
			// File provHistDir = createProvHistFolder();

			try {
				storeModelsForHistory(jobhistorypath, targetModel, runtimeModel);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		runtimeModel = downloadRuntimeModel();

		if (ModelUtility.isEqual(targetModel, runtimeModel)) {
			log.info("Provisioning Process successful");
			startPlatformDeployment(targetModel, runtimeModel);
		}

		if (jobhistorypath != null) {
			String modelPath = targetModel.getURI().path();
			Path p = Paths.get(modelPath);
			Path filename = p.getFileName();
			if (filename != null) {
				String jobName = Files.getNameWithoutExtension(filename.toString());
				String jobId = "job_" + startProvDate.getTime();
				DocciHistory jobHist = new DocciHistory(jobName, jobId, startProvDate, endProvDate, new Date(), this,
						"success");
				jobHist.storeHistory(jobhistorypath);
			}
		}

	}

	private void startPlatformDeployment(Resource targetModel, Resource runtimeModel) {
		try {
			log.info("Waiting for configuration of Infrastructure: " + configTime + "ms.");
			Thread.sleep(configTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		startRunningContainer(targetModel, runtimeModel);
		startApplicationDeployment(targetModel, runtimeModel);
	}

	private void startApplicationDeployment(Resource targetModel, Resource runtimeModel) {
		log.info("Starting Application Deployment");
		EList<org.eclipse.cmf.occi.core.Resource> applicationsToStart = getApplicationsToStart(targetModel,
				runtimeModel);
		MartAppDeployerMaster appDeployer = new MartAppDeployerMaster(conn);
		appDeployer.setJobHistoryPath(jobhistorypath);
		if (ModelUtility.getApplications(targetModel).isEmpty() == false
				&& inactiveApplications(targetModel, runtimeModel)) {
			EList<org.eclipse.cmf.occi.core.Resource> platformComponents = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
			platformComponents.addAll(ModelUtility.getApplications(targetModel));
			// appDeployer.deployApplications(platformComponents);
			appDeployer.deployApplications(applicationsToStart);
		} else {
			log.info("All Applications already active");
		}
		appDeployer.startAttachedComponents(runtimeModel);
	}

	private void startRunningContainer(Resource targetModel, Resource runtimeModel) {
		log.info("Start Containers");
		MartContainerRunnerMaster containerRunner = new MartContainerRunnerMaster(conn);
		containerRunner.setJobHistoryPath(jobhistorypath);
		containerRunner.runContainers(getContainersToStart(targetModel, runtimeModel));
	}

	private EList<org.eclipse.cmf.occi.core.Resource> getContainersToStart(Resource targetModel,
			Resource runtimeModel) {
		EList<org.eclipse.cmf.occi.core.Resource> cTS = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
		for (org.eclipse.cmf.occi.core.Resource targetContainer : ModelUtility.getContainers(targetModel)) {
			for (org.eclipse.cmf.occi.core.Resource runtimeContainer : ModelUtility.getContainers(runtimeModel)) {
				if (targetContainer.getId().equals(runtimeContainer.getId())) {
					Compute cont = (Compute) runtimeContainer;
					if (cont.getOcciComputeState().getValue() != ComputeStatus.ACTIVE_VALUE) {
						cTS.add(cont);
					}
				}
			}
		}
		return cTS;
	}

	private EList<org.eclipse.cmf.occi.core.Resource> getApplicationsToStart(Resource targetModel,
			Resource runtimeModel) {
		EList<org.eclipse.cmf.occi.core.Resource> aTS = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
		for (org.eclipse.cmf.occi.core.Resource targetApp : ModelUtility.getApplications(targetModel)) {
			for (org.eclipse.cmf.occi.core.Resource runtimeApp : ModelUtility.getApplications(runtimeModel)) {
				if (targetApp.getId().equals(runtimeApp.getId())) {
					Application app = (Application) runtimeApp;
					if (app.getOcciAppState().getValue() != org.modmacao.occi.platform.Status.ACTIVE.getValue()) {
						if (isExecutable(app) == false) {
							aTS.add(app);
						}
					}
				}
			}
		}
		return aTS;
	}

	private boolean isExecutable(Application app) {
		boolean isExecutable = false;
		for (Link rLink : app.getRlinks()) {
			if (rLink.getKind().getTerm().toLowerCase().equals("executionlink")) {
				isExecutable = true;
			}
			if (rLink.getKind().getTerm().toLowerCase().equals("platformdependency")) {
				return false;
			}
		}
		if (isExecutable) {
			return true;
		} else {
			return false;
		}
	}

	private boolean inactiveApplications(Resource targetModel, Resource runtimeModel) {
		for (org.eclipse.cmf.occi.core.Resource targetApp : ModelUtility.getApplications(targetModel)) {
			boolean newApplication = true;
			for (org.eclipse.cmf.occi.core.Resource runtimeApp : ModelUtility.getApplications(runtimeModel)) {
				if (targetApp.getId().equals(runtimeApp.getId())) {
					newApplication = false;
					Application app = (Application) runtimeApp;
					if (app.getOcciAppState().getValue() != org.modmacao.occi.platform.Status.ACTIVE.getValue()) {
						return true;
					}
				}
			}
			if (newApplication) {
				return true;
			}
		}
		return false;
	}

	private void registerUserMixins(Resource targetModel) {
		log.info("Registering Unregistered User Mixins");
		EList<Mixin> unregisteredMixins = getUnregisteredMixins(targetModel);
		registerMixins(unregisteredMixins);

	}

	private Resource downloadRuntimeModel() {
		Resource runtimeModel = null;
		boolean success = false;
		int retries = 0;
		while (success == false && retries < 3) {
			try {
				log.info("Downloading Runtime Model");
				conn.loadRuntimeModel(RUNTIMEPATH);
			} catch (RuntimeException e) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// e1.printStackTrace();
				}
				log.info("Downloading runtime model failed! Retrying!");
				retries++;
			}

			try {
				log.debug("Storing Runtime Model at: " + RUNTIMEPATH.toString());
				runtimeModel = ModelUtility.loadOCCIintoEMFResource(RUNTIMEPATH);

				if (containsUUIDString(ModelUtility.getOCCIConfiguration(runtimeModel)) == false) {
					log.warn("UUID String missing in runtime model. Applying Transformation!");
					// Sometimes the martserver returns the model without urn:uuid
					// To have a consistent behaviour, we always perform a transformation of
					// the downloaded model, and the urn:uuid substring is crucial for the
					// comparison
					// and thus for the deployment process.
					Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
					trans.transform(RUNTIMEPATH, RUNTIMEPATH);
					runtimeModel = ModelUtility.loadOCCIintoEMFResource(RUNTIMEPATH);
				}

				success = true;
			} catch (RuntimeException | EolRuntimeException e) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// e1.printStackTrace();
				}
				log.info("Could not read runtime model! Retrying!");
				e.printStackTrace();
				retries++;
			}

		}
		return runtimeModel;
	}

	private boolean containsUUIDString(Configuration occiConfiguration) {
		if (occiConfiguration.getResources().size() > 0) {
			if (occiConfiguration.getResources().get(0).getId().contains("urn:uuid:")) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	public void deploy(Path targetConfigPath) {
		org.eclipse.emf.ecore.resource.Resource targetModel = ModelUtility.loadOCCIintoEMFResource(targetConfigPath);
		deploy(targetModel);
	}

	private void adapt(org.eclipse.emf.ecore.resource.Resource runtimeModel,
			org.eclipse.emf.ecore.resource.Resource targetModel) {
		EList<EObject> occiModel = ModelUtility.getOCCIConfigurationContents(targetModel);
		// Compare Models
		CachedResourceSet.getCache().clear();
		Comparator comparator = ComparatorFactory.getComparator(COMP_TYPE, runtimeModel, targetModel);

		Executor exec = ExecutorFactory.getExecutor(EXEC_TYPE, conn);

		// Deprovision Missing Elements
		Deprovisioner deprovisioner = new Deprovisioner(exec, MANAGEMENTNETWORK);
		deprovisioner.setJhs(jobhistorypath);
		deprovisioner.deprovision(comparator.getMissingElements());

		// Adapt adapted elements
		// ElementAdapter adapter = new ElementAdapter(exec);
		// adapter.update(comparator.getAdaptedElements(), comparator.getMatches());

		// Create Provisioning Plan
		List<EObject> removeFromPOG = new BasicEList<EObject>();
		removeFromPOG.addAll(comparator.getOldElements());
		removeFromPOG.addAll(comparator.getAdaptedElements());
		Model provisioningPlan = generateProvisioningPlan(targetModel, removeFromPOG);

		// Start provisioning
		Provisioner provisioner = ProvisionerFactory.getProvisioner(PROV_TYPE,
				new ModelUtility().findInitialNode(provisioningPlan), occiModel, exec, null);
		provisioner.setJobHistoryPath(jobhistorypath);
		provisioner.provisionElements();

		log.info("Deployment process finished");
	}

	private void initialDeploy(org.eclipse.emf.ecore.resource.Resource targetModel) {
		CachedResourceSet.getCache().clear();
		EList<EObject> occiModel = ModelUtility.getOCCIConfigurationContents(targetModel);
		// OCCI2POG
		Transformator occiToPog = TransformatorFactory.getTransformator("OCCI2POG");
		try {
			occiToPog.transform(targetModel, POGPATH);
		} catch (EolRuntimeException e) {
			log.error("OCCI model could not be transformed into a POG " + e);
		}
		// POG2ProvPlan
		Transformator pogToProvPlan = TransformatorFactory.getTransformator("POG2ProvPlan");
		try {
			pogToProvPlan.transform(POGPATH, PROVPLANPATH);
		} catch (EolRuntimeException e) {
			log.error("POG model could not be transformed into a Provisioning plan " + e);
		}
		// LoadProvPlan
		Model provisioningPlan = ModelUtility.loadUML(PROVPLANPATH);
		// START PROVISIONING
		Executor exec = ExecutorFactory.getExecutor(EXEC_TYPE, conn);

		Provisioner provisioner = ProvisionerFactory.getProvisioner(PROV_TYPE,
				new ModelUtility().findInitialNode(provisioningPlan), occiModel, exec, null);
		provisioner.setJobHistoryPath(jobhistorypath);
		provisioner.provisionElements();

		log.info("Deployment process finished");
	}

	/**
	 * Returns usermixins from the OCCI targetModel which are not registered.
	 *
	 * @param targetModel OCCI Model containing the usermixins to be checked.
	 * @return unregistered usermixins.
	 */
	protected EList<Mixin> getUnregisteredMixins(Resource targetModel) {
		List<String> registeredMixins = new ArrayList<String>();
		EList<Mixin> unregistered = new BasicEList<Mixin>();

		MartExecutor exec = new MartExecutor(conn);
		String output = exec.getUsermixins();

		JSONObject json = new JSONObject(output);
		JSONArray jsonArr = json.getJSONArray("model");
		for (int i = 0; i < jsonArr.length(); i++) {
			String id = jsonArr.getJSONObject(i).getString("id");
			if (id.equals("http://schemas.modmacao.org/usermixins#")) {
				JSONObject jsonUserMixins = jsonArr.getJSONObject(i);
				JSONArray jsonUserMixinsArr = jsonUserMixins.getJSONArray("mixins");
				for (int k = 0; k < jsonUserMixinsArr.length(); k++) {
					String term = jsonUserMixinsArr.getJSONObject(k).getString("term");
					registeredMixins.add(term);
					log.debug("User Mixin: " + term + " registered.");
				}
			}
		}

		Configuration conf = ModelUtility.getOCCIConfiguration(targetModel);

		for (Mixin mix : conf.getMixins()) {
			if (mix.getScheme().equals("http://schemas.modmacao.org/usermixins#")) {
				if (registeredMixins.contains(mix.getTerm()) == false) {
					unregistered.add(mix);
					log.info("User Mixin: " + mix.getTerm() + " not registered.");
				}
			}
		}
		return unregistered;
	}

	/**
	 * Registers the passed usermixins.
	 *
	 * @param unregisteredMixins Mixins not already registered.
	 */
	public void registerMixins(EList<Mixin> unregisteredMixins) {
		for (Mixin mix : unregisteredMixins) {
			log.info("Registering Mixin: " + mix.getTerm());
			uploadMixin(mix);
			registerMixin(mix);
		}
	}

	private void uploadMixin(Mixin mix) {
		log.info("TODO: ASK USER TO UPLOAD MIXIN");
		// TODO: ASK USER TO UPLOAD MIXIN
	}

	private void registerMixin(Mixin mix) {
		MartExecutor exec = new MartExecutor(conn);
		exec.executeOperation("POST", mix, null);
	}
}
