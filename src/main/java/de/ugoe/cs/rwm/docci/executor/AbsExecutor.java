/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.executor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.eclipse.cmf.occi.core.Entity;

import de.ugoe.cs.rwm.docci.connector.Connector;

/**
 * Contains multiple methods required by every Executor
 *
 * @author erbel
 *
 */
public abstract class AbsExecutor implements Executor {
	protected Connector connector;
	protected Integer maxTries;
	private static final String ENCODINGCHARSET = "UTF-8";

	/**
	 * Establish a HttpURLConnection to the given address using the given REST
	 * method, output, contentType and authToken.
	 *
	 * @param address
	 *            Address of the REST interface.
	 * @param method
	 *            CRUD operation to be used.
	 * @param output
	 *            Boolean to define doOutput.
	 * @param contentType
	 *            Type of content.
	 * @param authToken
	 *            Authentication token to be used.
	 * @return HttpURLConnection for further adjustment
	 */
	@Deprecated
	protected HttpURLConnection establishConnection(String address, String method, Boolean output, String contentType,
			String authToken) {
		try {
			URL url = new URL(address);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(output);
			if (method != null) {
				conn.setRequestMethod(method);
			}
			if (contentType != null) {
				conn.setRequestProperty("Content-Type", contentType);
			}
			if (authToken != null) {
				conn.setRequestProperty("X-Auth-token", authToken);
			}
			return conn;

		} catch (MalformedURLException e) {
			LOG.error("Malformed URL: " + address);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Establish a HttpURLConnection to the given address using the given REST
	 * method, output, contentType and authToken.
	 *
	 * @param address
	 *            Address of the REST interface.
	 * @param method
	 *            CRUD operation to be used.
	 * @param output
	 *            Boolean to define doOutput.
	 * @param contentType
	 *            Type of content.
	 * @return HttpURLConnection for further adjustment
	 */
	protected HttpURLConnection establishConnection(String address, String method, Boolean output, String contentType) {
		try {
			URL url = new URL(address);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(output);
			if (contentType != null) {
				conn.setRequestProperty("Content-Type", contentType);
			}
			if (method != null) {
				conn.setRequestMethod(method);
			}
			return conn;

		} catch (MalformedURLException e) {
			LOG.error("Malformed URL: " + address);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Checks whether the connection is successful.
	 *
	 * @param conn
	 *            HttpURLConnection to be checked.
	 * @return Boolean whether connection is successful.
	 */
	protected boolean connectionSuccessful(HttpURLConnection conn) {
		try {
			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED
						&& conn.getResponseCode() != HttpURLConnection.HTTP_OK
						&& conn.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			conn.disconnect();
		}
		return true;
	}

	protected String processServerResponse(HttpURLConnection conn) {
		String output = getOutput(conn).replace("\n", "");
		String message = "Could not retrieve message";
		int code = -1;
		try {
			message = conn.getResponseMessage();
			code = conn.getResponseCode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (connectionSuccessful(conn)) {
			conn.disconnect();
			LOG.debug(code + ": "+ message + "! " + output);
			return output;
		} else {
			LOG.error("Request failed ("+ code + ": " + message +"): " + output);
			if(code == 400){
				conn.disconnect();
				return "Bad request: " + output;
			} else if(output.contains("Cannot find the source of the link")) {
				conn.disconnect();
				return "Resource containing the link could not be found";
			}
			conn.disconnect();
			return null;
		}
	}

	/**
	 * Writes the String input as input into the HttpUrlConnection conn.
	 *
	 * @param conn
	 *            HttpURLConnection to be used.
	 * @param input
	 *            Input for the Connection conn.
	 */
	protected void writeInput(HttpURLConnection conn, String input) {
		try {
			OutputStream os;
			os = conn.getOutputStream();
			os.write(input.getBytes(ENCODINGCHARSET));
			os.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Returns the output from the connection as concatenated String. On fault a
	 * null is returned.
	 *
	 * @param conn
	 *            HttpURLConnection to be read.
	 * @return Output of th HTTP Connection.
	 */
	protected String getOutput(HttpURLConnection conn) {
		int responseCode = getResponseCode(conn);
		if (200 <= responseCode && responseCode <= 299) {
			return getInputStream(conn);

		} else {
			return getErrorStream(conn);

		}
	}

	private String getErrorStream(HttpURLConnection conn) {
		InputStreamReader isr = null;
		StringWriter writer = new StringWriter();
		try {
			if (conn.getErrorStream() != null) {
				isr = new InputStreamReader(conn.getErrorStream(), ENCODINGCHARSET);
				IOUtils.copy(isr, writer);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return writer.toString();

		/*
		 * StringBuffer sb = new StringBuffer(); if (is != null) { InputStreamReader isr
		 * = null; BufferedReader br = null; try { isr = new InputStreamReader(is); br =
		 * new BufferedReader(isr); sb = new StringBuffer(); String inputLine = "";
		 * while ((inputLine = br.readLine()) != null) { sb.append(inputLine); }
		 * LOG.info("ERRORSTREAM: " + sb); return sb.toString(); } catch (IOException
		 * e1) { LOG.info("Could not retrieve error stream"); return ""; } finally { if
		 * (br != null) { try { br.close(); } catch (IOException e) {
		 * LOG.info("Could not close BufferedReader while retrieving errorstream");
		 * e.printStackTrace(); } }
		 *
		 * if (isr != null) { try { isr.close(); } catch (IOException e1) {
		 * LOG.info("Could not close InputStreamReader while retrieving errorstream"); }
		 * } } } return null;
		 */
	}

	private String getInputStream(HttpURLConnection conn) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), ENCODINGCHARSET))) {
			StringBuffer sb = new StringBuffer();
			String inputLine = "";
			while ((inputLine = br.readLine()) != null) {
				sb.append(inputLine);
			}
			return sb.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private int getResponseCode(HttpURLConnection conn) {
		int code;
		try {
			code = conn.getResponseCode();
		} catch (IOException e) {
			code = 0;
			e.printStackTrace();
		}
		return code;
	}

	/**
	 * Adapts address, so that the address contains the path of OCCI element to be
	 * created. E.g. Adds compute/storage or network to the path.
	 *
	 * @param entity
	 *            to be provisioned.
	 * @return adapted address.
	 */
	protected String getEntityKindURI(Entity entity) {
		// String address = Deployer.adress;
		String address = this.connector.getAddress();
		if (entity.getKind().getTerm().equals("networkinterface")) {
			address += "/networklink/";
		} else {
			address += "/" + entity.getKind().getTerm() + "/";
		}
		return address;
	}

	/**
	 * Logs the respones of a request stored in the passed String output.
	 *
	 * @param output
	 *            Output of the REST response.
	 */
	protected void logResponseOfRequest(String output) {
		LOG.debug("Rest response: " + output);
	}

	/**
	 * Returns Connector of the Executor object.
	 *
	 * @return connector Current Connector of the Executor object.
	 */
	public Connector getConn() {
		return connector;
	}

	/**
	 * Sets Connector of the Executor object.
	 *
	 * @param conn
	 *            Desired Connector of the Executor object.
	 */
	public void setConn(Connector conn) {
		this.connector = conn;
	}
}
