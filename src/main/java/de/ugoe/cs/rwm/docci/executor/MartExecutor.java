/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.docci.executor;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.ProxyCrossReferencer;
import org.eclipse.ocl.pivot.values.InvalidValueException;
import org.json.JSONArray;
import org.json.JSONObject;

import de.ugoe.cs.rwm.docci.connector.Connector;

/**
 * Handles execution of OCCI Model Elements.
 *
 * @author erbel
 *
 */
public class MartExecutor extends AbsExecutor {

	private static final int REREQUEST_SLEEP = 2000;

	/**
	 * Creates an Executor to the OCCI API of the specified connection. Sets
	 * maxTries to 3.
	 *
	 * @param conn
	 */
	public MartExecutor(Connector conn) {
		this.connector = conn;
		this.maxTries = 3;
	}

	/**
	 * Creates Executor to the OCCI API of the specified connection. maxTries is
	 * hereby the maximum amount of retries for a request. Should be at least 2 to
	 * handle connection issues.
	 *
	 * @param conn
	 * @param maxTries
	 */
	public MartExecutor(Connector conn, int maxTries) {
		this.connector = conn;
		this.maxTries = maxTries;
	}

	@Override
	public String executeOperation(String operation, EObject element, EObject action) {
		boolean success = false;
		String output = "";
		int count = 0;

		while (success == false && count < maxTries) {
			if (operation.equals("POST") && action == null) {
				output = executePostOperation(element);
			} else if (operation.equals("PUT")) {
				output = executePutOperation(element);
			} else if (operation.equals("GET")) {
				output = executeGetOperation(element);
			} else if (operation.equals("DELETE")) {
				output = executeDeleteOperation(element);
			} else if (operation.equals("POST") && action != null) {
				output = executeActionOperation(element, action);
			}

			// TODO: Output kommt eigentlich immer;
			if (output != null) {
				success = true;
			}

			if (success == false) {
				count++;
				try {
					LOG.info(operation + " Failed: " + ((Entity) element).getTitle() + " Rerequest in "
							+ REREQUEST_SLEEP + "ms!");

					// Shift attributes (Workaround for MartServer Mixin resolution strategy)
					shiftMixinBaseAttributes(element);

					Thread.sleep(REREQUEST_SLEEP);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return output;
	}

	private void shiftMixinBaseAttributes(EObject element) {
		EList<AttributeState> toSwap = new BasicEList<AttributeState>();
		Entity ent = (Entity) element;
		for (MixinBase part : ent.getParts()) {
			for (AttributeState attr : part.getAttributes()) {
				for (EStructuralFeature feat : part.eClass().getEAllStructuralFeatures()) {
					if (attr.getName().equals(feat.getName())) {
						LOG.debug("Shifting attribute to position 0: " + attr);
						toSwap.add(attr);
					}
				}
			}
			for (AttributeState attr : toSwap) {
				if (part.getAttributes().contains(attr)) {
					part.getAttributes().move(0, attr);
				}
			}
		}
	}

	/**
	 * Performs a POST(Action) request on the EObject element with the Action stored
	 * in EObject eAction.
	 *
	 * @param element
	 * @param eAction
	 * @return
	 */
	private String executeActionOperation(EObject element, EObject eAction) {
		Entity entity = (Entity) element;
		Action action = (Action) eAction;

		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + getLocation(entity);
		adaptedAddress += "?action=";
		adaptedAddress += action.getTerm();
		HttpURLConnection conn = establishConnection(adaptedAddress, "POST", true, "text/occi");

		conn.setRequestProperty("Category", generateCategoryHeader(action));
		conn.setRequestProperty("Accept", "application/json");
		LOG.info("POST " + conn.getURL() + " -H 'Content-Type: " + conn.getRequestProperty("Content-Type") + "' "
				+ " -H 'Category: " + conn.getRequestProperty("Category") + "' ");

		return processServerResponse(conn);
	}

	private String getLocation(Entity ent) {
		if (ent.getLocation() != null && ent.getLocation().isEmpty() == false) {
			return ent.getLocation();
		} else {
			StringBuilder location = new StringBuilder();
			location.append("/");
			location.append(ent.getKind().getTerm());
			location.append("/");
			location.append(ent.getId());
			location.append("/");
			return location.toString();
		}
	}

	/**
	 * Returns generated Category header for the passed action.
	 *
	 * @param action
	 * @return
	 */
	private String generateCategoryHeader(Action action) {
		String category = action.getTerm() + "; " + "scheme=\"" + action.getScheme() + "\"; " + "class=\"action\"";
		return category;
	}

	/**
	 * Returns output of GET request for the passed Entity EObject extracted.
	 *
	 * @param extracted
	 * @return Output of the GET request.
	 */
	public String executeGetOperation(EObject extracted) {
		Entity entity = (Entity) extracted;
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + getLocation(entity);
		HttpURLConnection conn = establishConnection(adaptedAddress, null, false, null);
		conn.setRequestProperty("Accept", "application/json");

		LOG.info("GET" + " " + adaptedAddress);

		return processServerResponse(conn);
	}

	/**
	 * Issues a cloud session token required for authorization.
	 *
	 * @param user
	 * @param password
	 * @param project
	 * @param address
	 * @return cloud session token
	 */
	@Deprecated
	public String createToken(String user, String password, String project, String address) {
		return "";
	}

	/**
	 * Performs POST request to provision the Resource described by the EObject
	 * element.
	 *
	 * @param element
	 * @return
	 */
	private String executePostOperation(EObject element) {
		if (element instanceof Entity) {
			Entity entity = (Entity) element;
			return postEntity(entity);
		} else if (element instanceof Mixin) {
			Mixin mixin = (Mixin) element;
			if (mixin.getScheme().equals("http://schemas.modmacao.org/usermixins#")) {
				return postUserMixin(mixin);
			}
		}
		throw new IllegalArgumentException("Can not post element: " + element);

	}

	private String postUserMixin(Mixin mixin) {
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + "/-/";
		HttpURLConnection conn = establishConnection(adaptedAddress, "POST", true, "text/occi");
		conn.setRequestProperty("Category", generateUserMixinHeader(mixin));
		conn.setRequestProperty("Accept", "application/json");
		LOG.info("POST" + " " + conn.getURL() + " -H 'Content-Type: " + conn.getRequestProperty("Content-Type") + "' "
				+ " -H 'Category: " + conn.getRequestProperty("Category") + "' ");

		return processServerResponse(conn);
	}

	private String generateUserMixinHeader(Mixin mixin) {
		String category = "";
		category = mixin.getTerm() + "; " + "scheme=\"" + mixin.getScheme() + "\"; " + "class=\"mixin\"; "
				+ "location=\"/usermixins/\"";
		return category;
	}

	private String postEntity(Entity entity) {
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + getLocation(entity);
		if (hasJSONAttributes(entity) == false) {
			HttpURLConnection conn = establishConnection(adaptedAddress, "POST", true, "text/occi");
			conn.setRequestProperty("Category", generateCategoryHeader(entity));
			conn.setRequestProperty("X-OCCI-Attribute", generateAttributeHeader(entity));
			conn.setRequestProperty("Accept", "application/json");
			LOG.info("POST " + conn.getURL() + " -H 'Content-Type: " + conn.getRequestProperty("Content-Type") + "' "
					+ " -H 'Category: " + conn.getRequestProperty("Category") + "' " + " -H 'X-OCCI-Attribute:"
					+ conn.getRequestProperty("X-OCCI-Attribute") + "' ");

			return processServerResponse(conn);
		} else {
			return performPostApplicationJson(entity);
		}
	}

	private String performPostApplicationJson(Entity entity) {
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + getLocation(entity);
		HttpURLConnection conn = establishConnection(adaptedAddress, "POST", true, "application/json");
		conn.setRequestProperty("Accept", "application/json");
		String jsonRepresentation = createJson(entity);

		try {
			byte[] postDataBytes = jsonRepresentation.getBytes("UTF-8");

			LOG.debug("Comma within attributes detected! Entity: " + entity);
			LOG.info("POST -d ' " + jsonRepresentation + " ' " + conn.getURL() + " -H 'Content-Type: "
					+ conn.getRequestProperty("Content-Type") + "' ");

			conn.getOutputStream().write(postDataBytes);

			return processServerResponse(conn);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Performs DELETE request to deprovision Resource described by the EObject
	 * element.
	 *
	 * @param element
	 * @return
	 */
	private String executeDeleteOperation(EObject element) {
		Entity entity = (Entity) element;
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + getLocation(entity);
		HttpURLConnection conn = establishConnection(adaptedAddress, "DELETE", false, null);
		conn.setRequestProperty("Accept", "application/json");
		LOG.info("DELETE" + " " + conn.getURL());
		return processServerResponse(conn);
	}

	/**
	 * Generates X-OCCI-Attribute header for the HttpURLConnection.
	 *
	 * @param entity containing the Attributes.
	 * @return String containing the X-OCCI-Attribute Header.
	 */
	private String generateAttributeHeader(Entity entity) {
		StringBuffer attributes = new StringBuffer();

		if (entity instanceof Link) {
			Link link = (Link) entity;
			if (link.getSource() != null) {
				attributes.append("occi.core.source=\"" + getLocation(link.getSource()) + "\",");
			} else {
				for (AttributeState state : entity.getAttributes()) {
					if (state.getName().equals("occi.core.source")) {
						attributes.append(state.getName() + "=\"" + state.getValue() + "\", ");
					}
				}
			}
			if (link.getTarget() != null) {
				attributes.append("occi.core.target=\"" + getLocation(link.getTarget()) + "\",");
			} else {
				for (AttributeState state : entity.getAttributes()) {
					if (state.getName().equals("occi.core.target")) {
						attributes.append(state.getName() + "=\"" + state.getValue() + "\", ");
					}
				}
			}
		}

		for (AttributeState state : entity.getAttributes()) {
			if ((state.getName().equals("occi.core.target") || state.getName().equals("occi.core.source")) == false) {
				attributes.append(state.getName() + "=\"" + state.getValue() + "\", ");
			}
		}

		for (MixinBase base : entity.getParts()) {
			for (AttributeState baseState : base.getAttributes()) {
				attributes.append(baseState.getName() + "=\"" + baseState.getValue() + "\", ");
			}
		}
		if (attributes.length() == 0) {
			LOG.warn("Entity: " + entity + " seams to not contain any attributes.");
			return "";
		}
		return attributes.substring(0, attributes.lastIndexOf(","));
	}

	/**
	 * Generates Category header for the HttpURLConnection.
	 *
	 * @param entity containing the Attributes.
	 * @return String containing the Category Header.
	 */
	private String generateCategoryHeader(Entity entity) {
		StringBuffer category = new StringBuffer();

		if (ProxyCrossReferencer.find(entity).isEmpty() == false) {
			StringBuilder proxies = new StringBuilder();
			proxies.append("Proxy Detected on Entity: " + entity.getTitle());
			if (entity.getKind().eIsProxy()) {
				proxies.append(" | Kind: " + entity.getKind());
			}

			for (Mixin mixin : entity.getMixins()) {
				proxies.append(" | Mixins: ");
				if (mixin.eIsProxy()) {
					proxies.append(mixin);
					EcoreUtil.resolveAll(mixin);
				}
			}
			LOG.info(proxies);
			EcoreUtil.resolveAll(entity);
			LOG.info("Remaining proxies after resolution: " + ProxyCrossReferencer.find(entity).isEmpty());
		}

		try {
			category.append(entity.getKind().getTerm() + "; " + "scheme=\"" + entity.getKind().getScheme() + "\"; "
					+ "class=\"kind\"");
		} catch (InvalidValueException e) {
			LOG.error("Kind: " + entity.getKind() + " could not be appended to entity. " + entity);
			e.printStackTrace();
		}
		for (Mixin mixin : entity.getMixins()) {
			try {
				category.append(
						", " + mixin.getTerm() + "; " + "scheme=\"" + mixin.getScheme() + "\"; " + "class=\"mixin\"");
			} catch (InvalidValueException e) {
				LOG.error("Mixin: " + mixin + " could not be appended correctly to entity. " + entity);
			}
		}

		return category.toString();

	}

	/**
	 * Executes PUT requested based on the Resource described in the EObject
	 * element.
	 *
	 * @param element Element to PUT
	 * @return Connection Output.
	 */
	public String executePutOperation(EObject element) {
		Entity entity = (Entity) element;
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + getLocation(entity);
		if (hasJSONAttributes(entity) == false) {
			HttpURLConnection conn = establishConnection(adaptedAddress, "PUT", true, "text/occi");
			conn.setRequestProperty("Category", generateCategoryHeader(entity));
			conn.setRequestProperty("X-OCCI-Attribute", generateAttributeHeader(entity));
			conn.setRequestProperty("Accept", "application/json");

			LOG.info("PUT " + conn.getURL() + " -H 'Content-Type: " + conn.getRequestProperty("Content-Type") + "' "
					+ " -H 'Category: " + conn.getRequestProperty("Category") + "' " + " -H 'X-OCCI-Attribute:"
					+ conn.getRequestProperty("X-OCCI-Attribute") + "' ");

			return processServerResponse(conn);
		} else {
			return performPutApplicationJsonRequest(entity);
		}
	}

	private String performPutApplicationJsonRequest(Entity entity) {
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + getLocation(entity);
		HttpURLConnection conn = establishConnection(adaptedAddress, "PUT", true, "application/json");
		conn.setRequestProperty("Accept", "application/json");
		String jsonRepresentation = createJson(entity);

		try {
			byte[] postDataBytes = jsonRepresentation.getBytes("UTF-8");

			LOG.debug("Comma within attributes detected! Entity: " + entity);
			LOG.info("PUT -d ' " + jsonRepresentation + " ' " + conn.getURL() + " -H 'Content-Type: "
					+ conn.getRequestProperty("Content-Type") + "' ");

			conn.getOutputStream().write(postDataBytes);

			return processServerResponse(conn);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String createJson(Entity entity) {
		JSONObject json = new JSONObject();
		try {
			json.put("id", entity.getId());
			json.put("title", entity.getTitle());
			json.put("kind", entity.getKind().getScheme() + entity.getKind().getTerm());
			json.put("location", getLocation(entity));
			json.put("attributes", createJsonAttributes(entity));
			json.put("mixins", createJsonParts(entity));

			if (entity instanceof Link) {
				Link link = (Link) entity;
				JSONObject source = new JSONObject();
				source.put("location", getLocation(link.getSource()));
				json.put("source", source);

				JSONObject target = new JSONObject();
				target.put("location", getLocation(link.getTarget()));
				json.put("target", target);
			}
		} catch (InvalidValueException e) {
			LOG.fatal("Body of entity:" + entity.getTitle()
					+ " could not be created properly due to a missloaded OCCI extension.");
		}
		String jsonString = json.toString();
		return jsonString;
	}

	private JSONArray createJsonParts(Entity ent) {
		JSONArray arr = new JSONArray();
		for (Mixin mix : ent.getMixins()) {
			arr.put(mix.getScheme() + mix.getTerm());
		}
		return arr;

	}

	private JSONObject createJsonAttributes(Entity ent) {
		JSONObject obj = new JSONObject();
		for (AttributeState attr : ent.getAttributes()) {
			obj.put(attr.getName(), attr.getValue());
		}
		for (MixinBase mixB : ent.getParts()) {
			for (AttributeState attr : mixB.getAttributes()) {
				obj.put(attr.getName(), attr.getValue());
			}
		}

		return obj;

	}

	private boolean hasJSONAttributes(Entity entity) {
		for (AttributeState attr : entity.getAttributes()) {
			if (attr.getValue().contains(",") || attr.getValue().contains("\"") || attr.getValue().contains("=")) {
				return true;
			}
		}
		for (MixinBase mixB : entity.getParts()) {
			for (AttributeState attr : mixB.getAttributes()) {
				if (attr.getValue().contains(",") || attr.getValue().contains("\"") || attr.getValue().contains("=")) {
					return true;
				}
			}
		}
		return false;

	}

	/**
	 * Returns registered usermixins.
	 *
	 * @return String containing the REST response when asking for usermixins.
	 */
	public String getUsermixins() {
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + "/-/";
		HttpURLConnection conn = establishConnection(adaptedAddress, null, false, null);
		conn.setRequestProperty("Accept", "application/json");

		LOG.debug("GET" + " " + adaptedAddress);
		String output = getOutput(conn);
		// LOG.debug(output);
		return output;
	}

	/**
	 * Simple GET request against: http://address:port'query'
	 *
	 * @param query
	 * @return String containing the REST response when asking for usermixins.
	 */
	public String executeGetOperation(String query) {
		String adaptedAddress = "http://" + connector.getAddress() + ":" + connector.getPort() + query;
		HttpURLConnection conn = establishConnection(adaptedAddress, null, false, null);
		conn.setRequestProperty("Accept", "application/json");

		LOG.info("GET" + " " + adaptedAddress);

		return processServerResponse(conn);
	}
}
